import express, { Application } from 'express';
import http from 'http';
import cors from 'cors';
import path from 'path';
import db from '../database/config';
import auth from '../router/auth';
import empleados from '../router/empleados';
import area from '../router/area';
import pais from '../router/pais';
import tipodocumento from '../router/tipodocumento';

class Server {
    
    private app: Application;
    private port: string;
    private server: http.Server;

    constructor(){
        this.app = express();
        this.port = process.env.PORT || '8080';

        this.dbConnection();
        this.server = http.createServer(this.app);
    }

    async dbConnection(){
        try {
            await db.authenticate();
            // El force en true dropea todas las tablas y las vuelve a crear
            // await db.sync({ force: true });
            await db.sync();
            console.log('Database online'.blue);
        } catch (error) {
            console.log('Error: '.red,error);
        }
    }

    execute() {

        // Inicializar Middlewares
        this.middlewares();

        // Inicializar Server
        this.server.listen( this.port, () => {
            console.log('Server corriendo en puerto:', this.port );
        });
    }

    middlewares(){
        // Desplegar el directorio público
        this.app.use( express.static( path.resolve( __dirname, '../public' ) ) );
        // Permite las peticiones HTTP
        try {
            this.app.use(
                cors()
            );
        } catch (error) {
            console.log(error);
        }

        // Parseo del body
        this.app.use( express.json() );
        this.routes();
    }

    // Aca se especifican todos los endpoints del servicio REST
    routes() {
        this.app.use('/api/auth', auth);
        this.app.use('/api/area', area);
        this.app.use('/api/pais', pais);
        this.app.use('/api/empleados', empleados);
        this.app.use('/api/tipodocumento', tipodocumento);
    }

}

export default Server;