import { 
    AllowNull,
    AutoIncrement,
    Column,
    IsEmail,
    Model,
    NotEmpty,
    PrimaryKey,
    Table,
    Unique,
} from 'sequelize-typescript';

export interface UsuarioI{
    email: string;
    password: string;
}

@Table({
    tableName: 'Usuario',
    timestamps: false,
})
// Esta clase no esta explicita en el enunciado, pero la hice
// Para simular un manejo del usuario administrador que registra
// Los empleados.
export default class Usuario extends Model implements UsuarioI {

    @AutoIncrement
    @PrimaryKey
    @Column
    id!: number;

    @AllowNull(false)
    @NotEmpty
    @IsEmail
    @Unique
    @Column
    email!: string;

    @AllowNull(false)
    @NotEmpty
    @Column
    password!: string;
    
    public mapped(){
        return {
            id: this.id,
            email: this.email
        };
    }
}