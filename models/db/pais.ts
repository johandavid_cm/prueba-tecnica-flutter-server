import {
    AllowNull,
    AutoIncrement,
    Column,
    DataType,
    HasMany,
    Model,
    NotEmpty,
    NotNull,
    PrimaryKey,
    Table,
} from "sequelize-typescript";
import Empleado from "./empleado";

export interface PaisI {
    nombre: string;
    codigo: string;
}

@Table({
    tableName: 'Pais',
    timestamps: false,
})
class Pais extends Model implements PaisI{

    @AutoIncrement
    @PrimaryKey
    @Column
    id!: number;

    @AllowNull(false)
    @NotEmpty
    @NotNull
    @Column(DataType.STRING(120))
    nombre!: string;

    @AllowNull(false)
    @NotEmpty
    @NotNull
    @Column(DataType.STRING(3))
    codigo!: string;

    @HasMany(() => Empleado)
    empleados!: Empleado[];

}

export default Pais;