import { 
    AllowNull,
    AutoIncrement,
    Column,
    DataType,
    HasMany,
    Model,
    NotEmpty,
    PrimaryKey,
    Table,
    Unique,
} from 'sequelize-typescript';
import Empleado from './empleado';

export interface TipoIdentificacionI{
    codigo: string;
    nombre: string;
}

@Table({
    tableName: 'TipoIdentificacion',
    timestamps: false,
})

export default class TipoIdentificacion extends Model implements TipoIdentificacionI {

    @AutoIncrement
    @PrimaryKey
    @Column
    id!: number;

    @AllowNull(false)
    @NotEmpty
    @Unique
    @Column(DataType.STRING(3))
    codigo!: string;

    @AllowNull(false)
    @NotEmpty
    @Column(DataType.STRING(120))
    nombre!: string;

    @HasMany(() => Empleado)
    empleados!: Empleado[]
}