import { 
    AllowNull,
    AutoIncrement,
    BelongsTo,
    Column,
    CreatedAt,
    DataType,
    Default,
    ForeignKey,
    Is,
    IsEmail,
    Model,
    NotEmpty,
    PrimaryKey,
    Table,
    Unique,
    UpdatedAt,
} from 'sequelize-typescript';
import Area from './area';
import Pais from './pais';
import TipoIdentificacion from './tipoidentificacion';

export interface EmpleadoI{
    email: string;
    primerNombre: string;
    otrosNombres?: string;
    primerApellido: string;
    segundoApellido: string;
    numeroIdentificacion: string;
    tipoIdentificacionId: number;
    areaId: number;
    paisId: number;
}


const PRIMER_APELLIDO_EXP: RegExp = /[A-Z]\s?/
const OTROS_NOMBRES_EXP: RegExp = /[A-Z]\s?/
const NUMERO_IDENTIFICACION_EXP: RegExp = /[a-zA-Z0-9]\-?/g

@Table({
    tableName: 'Empleado',
})

export default class Empleado extends Model implements EmpleadoI {

    // Los decoradores utilizados mediante el 
    // Sequelize-Typescript funcionan como constrainst ya establecidos
    // Agilizan bastante el desarrollo de la BD

    @AutoIncrement
    @PrimaryKey
    @Column
    id!: number;

    @AllowNull(false)
    @NotEmpty
    @Is('ValidarPrimerApellido', (value: string) => {
      if (!PRIMER_APELLIDO_EXP.test(value)) {
        throw new Error(`"${value}" no es un valor entre la A - Z para primer apellido`)
      }
    })
    @Column(DataType.STRING(20))
    primerApellido!: string;

    @AllowNull(false)
    @NotEmpty
    @Is('ValidarSegundoApellido', (value) => {
        if (!PRIMER_APELLIDO_EXP.test(value)) {
          throw new Error(`"${value}" no es un valor entre la A - Z para segundo apellido`)
        }
      })
    @Column(DataType.STRING(20))
    segundoApellido!: string;

    @AllowNull(false)
    @NotEmpty
    @Is('ValidarPrimerNombre', (value) => {
        if (!PRIMER_APELLIDO_EXP.test(value)) {
          throw new Error(`"${value}" no es un valor entre la A - Z para primer nombre`)
        }
      })
    @Column(DataType.STRING(20))
    primerNombre!: string;


    @Is('ValidarOtrosNombres', (value) => {
        if (value && !OTROS_NOMBRES_EXP.test(value)) {
          throw new Error(`"${value}" no es un valor entre la A - Z para otros nombres`)
        }
      })
    @AllowNull(true)
    @Column(DataType.STRING(50))
    otrosNombres?: string | undefined;

    @Is('ValidarOtrosNombres', (value) => {
    if (!NUMERO_IDENTIFICACION_EXP.test(value)) {
        throw new Error(`"${value}" no es una identificación no válida`)
      }
    })
    @Column(DataType.STRING(20))
    numeroIdentificacion!: string;
    
    @AllowNull(false)
    @NotEmpty
    @IsEmail
    @Unique
    @Column(DataType.STRING(300))
    email!: string;


    
    @AllowNull(false)
    @Default(true)
    @Column
    estado!: boolean;

    //Relacion 1 - M
    //Relacion con la identificacion
    @ForeignKey(() => TipoIdentificacion)
    @Column
    tipoIdentificacionId!: number;
    
    @BelongsTo(() => TipoIdentificacion)
    TipoIdentificacion!: TipoIdentificacion
    
    //Relacion 1 - M
    //Relacion con el area
    @ForeignKey(() => Area)
    @Column
    areaId!: number;

    //Relacion 1 - M
    //Relacion con el país
    @ForeignKey(() => Pais)
    @Column
    paisId!: number;

    @BelongsTo(() => Pais)
    pais!: Pais;

    @BelongsTo(() => Area)
    area!: Area;

    @CreatedAt
    fechaCreacion!: Date;

    @UpdatedAt
    fechaActualizacion!: Date;
}