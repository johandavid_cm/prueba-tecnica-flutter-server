import {
    AllowNull,
    AutoIncrement,
    Column,
    DataType,
    HasMany,
    Model,
    NotEmpty,
    NotNull,
    PrimaryKey,
    Table,
} from "sequelize-typescript";
import Empleado from "./empleado";

export interface AreaI {
    nombre: string;
}

@Table({
    tableName: 'Area',
    timestamps: false,
})
class Area extends Model implements AreaI{

    @AutoIncrement
    @PrimaryKey
    @Column
    id!: number;

    @AllowNull(false)
    @NotEmpty
    @NotNull
    @Column(DataType.STRING(120))
    nombre!: string;

    @HasMany(() => Empleado)
    empleados!: Empleado[];

}

export default Area;