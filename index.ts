// Contiene todo el servidor
import Server from './models/server';
import dotenv from 'dotenv';
dotenv.config();

const server = new Server();
// Ejecutar el server
server.execute();
