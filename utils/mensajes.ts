export const mensajesRespuesta = {
    status500: 'Ha ocurrido un error inesperado. Contacte con el administrador.',
    loginFailed: 'Email o contraseña incorrectos, por favor verifique',
    login200: 'Se ha iniciado sesión correctamente',
    emailRegistered: 'El correo ya esta registrado.'
}