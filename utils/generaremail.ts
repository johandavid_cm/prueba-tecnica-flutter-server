import { EmpleadoI } from "../models/db/empleado";
import Pais from "../models/db/pais";

export const generarEmail = (empleado: EmpleadoI, pais: Pais) => {
    return `${empleado.primerNombre}.${empleado.primerApellido}.${empleado.numeroIdentificacion}@${`cidenet.com.${pais.codigo}`}`.replace(/ /g, '').toLowerCase();
};