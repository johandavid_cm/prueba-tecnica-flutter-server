import { Request } from "express";

export const obtenerIdRequest = (req: Request) => {
    let oid: number = 0;
    for (const [clave, valor] of Object.entries(req)) {
        if(clave === 'id'){
            oid = valor;
        }
    }
    return oid;
};