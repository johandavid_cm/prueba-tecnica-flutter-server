/*
    path: /api/empleados
*/

import { Router } from "express";
import { check } from "express-validator";
import { validarCampos } from "../middlewares/validar-campos";
import { validarJWT } from "../middlewares/validar-jwt";
import {
    crearEmpleado,
    eliminarEmpleado,
    getEmpleados
} from "../controllers/empleados";

const router = Router();

router.post('/', [
    validarJWT,
    check('primerNombre', 'Primer nombre inválido').notEmpty().isLength({ max: 20 }),
    check('primerApellido', 'Primer apellido inválido').notEmpty().isLength({ max: 20 }),
    check('segundoApellido', 'Segundo apellido inválido').notEmpty().isLength({ max: 20 }),
    check('numeroIdentificacion', 'Número de identificación no válido').notEmpty().isLength({ max: 20 }),
    check('numeroIdentificacion', 'Número de identificación no válido').notEmpty().isLength({ max: 20 }),
    check('tipoIdentificacionId', 'Tipo de identificación inválida').isNumeric(),
    check('areaId', 'Area inválida').isNumeric(),
    check('paisId', 'País inválido').isNumeric(),
    validarCampos
], crearEmpleado);

router.delete('/:idEmpleado', [
    validarJWT
], eliminarEmpleado)

router.get('/', [
    validarJWT,
], getEmpleados);

export default router;