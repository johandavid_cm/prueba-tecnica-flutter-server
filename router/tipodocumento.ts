import { Router } from "express";
import { getTiposDocumento } from "../controllers/tipodocumento";
import { validarJWT } from "../middlewares/validar-jwt";

const router = Router();

router.get('/', [
    validarJWT
], getTiposDocumento)

export default router;