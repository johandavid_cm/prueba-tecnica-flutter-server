/*
    path: /api/auth
*/

import { Router } from "express";
import { crearUsuario, iniciarSesion, renew } from "../controllers/auth";
import { check } from 'express-validator';
import { validarCampos } from "../middlewares/validar-campos";
import { validarJWT } from "../middlewares/validar-jwt";

const router = Router();

router.post('/', [
    check('email', 'Email no válido').notEmpty().isEmail().isLength({ max: 300 }),
    check('password', 'Password no válido').notEmpty(),
    validarCampos
], iniciarSesion);

router.post('/crear', [
    check('email', 'Email no válido').notEmpty().isEmail().isLength({ max: 300 }),
    check('password', 'Password no válido').notEmpty(),
    validarCampos
], crearUsuario);

router.get('/renew', [
    validarJWT
], renew);

export default router;