/* 
    path: 'api/area'
*/

import { Router } from "express";
import { getAreas } from "../controllers/area";
import { validarJWT } from "../middlewares/validar-jwt";

const router = Router();

router.get('/',[
    validarJWT
], getAreas);

export default router;