import { Router } from "express";
import { getPaises } from "../controllers/pais";
import { validarJWT } from "../middlewares/validar-jwt";

const router = Router();

router.get('/', [
    validarJWT
], getPaises)

export default router;