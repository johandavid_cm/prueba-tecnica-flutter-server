# Prueba Tecnica Flutter Server

Servidor escrito en Node como parte de la prueba tecnica para Cidenet

## Instalación

Lo primero a tener en cuenta luego de realizar el `git clone es`:


1. Es recomendable tener el typescript de manera global en el sistema, de no tener typescript instalado simplemente ejecutar un **npm i -g typescript**.

2. Luego de instalar typescript ejecutar el comando `tsc` en la consola, este comando generará la carpeta de **dist** la cual es la compilada en javascript y la que iría a un servidor final.


3. Una vez realizado el `tsc` hay que tener presente en donde se creará la BD. la trabaje de manera local dado que con los proovedores que ofrecen un servicio gratuito la versión del motor de BD y la versión del sequelize no eran compatibles. Por eso la trabaje de manera local. La BD inicial esta con mysql pero al trabajar con el ORM de **sequelize**, este me permite migrar la BD a muchas otros motores sin trabajo alguno. solo en la carpeta **database/config.ts** en la constante **config** cambiar: el host, username, password y dialect por el deseado.

4. Y por último en la carpeta `database/script-inicia.sql` están los primeros scripts para que la app funcione correctamente

5. Luego de esta configuración ejecutar el `npm start` Este me iniciará el servidor y la app ya estará lista para hacer las peticiones.
