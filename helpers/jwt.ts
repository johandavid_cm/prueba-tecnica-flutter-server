import jwt from 'jsonwebtoken';

export const generarJWT = ( id: string, duracion:string = '24h', clave: string | undefined = process.env.JWT_KEY ) => {
    return new Promise<string | undefined>(( resolve, reject ) => {
        // Informacion que irá en el JSON Web Token
        // Entre menos mejor
        if(clave){
            const payload = { id };
            jwt.sign( payload, clave, {
                expiresIn: duracion
            }, (err, token) => {
                if( err ){
                    console.log(err);
                    reject('No se pudo generar el JWT');
                } else {
                    resolve( token );
                }
            });
        } else {
            reject('No se pudo generar el JWT');
        }
    });
};