import { Request, Response } from "express";
import Area from "../models/db/area";
import { mensajesRespuesta } from "../utils/mensajes";

export const getAreas = async(_req: Request, res: Response) => {
    try {
        const areas = await Area.findAll();
        return res.status(200).json({
            ok: true,
            msg: 'Se han obtenido las areas correctamente',
            areas,
        });
    } catch (error) {
        console.log(error);
        return res.status(500).json({
            ok: false,
            msg: mensajesRespuesta.status500
        });
    }
};