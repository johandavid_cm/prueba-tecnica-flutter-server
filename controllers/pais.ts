import { Request, Response } from "express";
import Pais from "../models/db/pais";
import { mensajesRespuesta } from "../utils/mensajes";

export const getPaises = async(_req: Request, res: Response) => {
    try {
        const paises = await Pais.findAll();
        return res.status(200).json({
            ok: true,
            msg: 'Se han obtenido los paises correctamente',
            paises,
        });
    } catch (error) {
        console.log(error);
        return res.status(500).json({
            ok: false,
            msg: mensajesRespuesta.status500
        });
    }
};