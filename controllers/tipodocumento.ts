import { Request, Response } from "express";
import TipoIdentificacion from "../models/db/tipoidentificacion";
import { mensajesRespuesta } from "../utils/mensajes";

export const getTiposDocumento = async(_req: Request, res: Response) => {
    try {
        const tiposDocumento = await TipoIdentificacion.findAll();
        return res.status(200).json({
            ok: true,
            msg: 'Se han obtenido los tipos de documento correctamente',
            tiposDocumento,
        });
    } catch (error) {
        console.log(error);
        return res.status(500).json({
            ok: false,
            msg: mensajesRespuesta.status500
        });
    }
};