import { Request, Response } from "express";
import bcrypt from 'bcryptjs';
import Usuario, { UsuarioI } from "../models/db/usuario";
import { generarJWT } from "../helpers/jwt";
import { mensajesRespuesta } from "../utils/mensajes";
import { obtenerIdRequest } from "../utils/obteneridrequest";

export const iniciarSesion = async (req: Request, res: Response) => {
    const { email, password } = req.body;
    // Verificamos que exista el usuario
    try {
        const usuario = await Usuario.findOne({
            where: {
                email
            }
        });
        // Se pone el mismo mensaje para no dar pistas de que dato falta
        if (!usuario) {
            return res.status(400).json({
                ok: false,
                msg: mensajesRespuesta.loginFailed
            });
        }
        // Comparamos la contraseña
        const validPassword = bcrypt.compareSync(password, usuario.password);
        if (!validPassword) {
            return res.status(400).json({
                ok: false,
                msg: mensajesRespuesta.loginFailed
            });
        }
        // Generamos el token
        const token = await generarJWT(usuario.id.toString());
        return res.status(200).json({
            ok: true,
            msg: mensajesRespuesta.login200,
            usuario: usuario.mapped(),
            token
        });
    } catch (error) {
        console.log(`${error}`.red);
        return res.status(500).json({
            ok: false,
            msg: mensajesRespuesta.status500
        });
    }
};


// Este controlador me creara el usuario "Administrativo"
// Repito como en la definición, no esta especificado
// Pero me parecio la mejor manera de hacerlo.
// En un escenario real tendría muchas más validaciones
export const crearUsuario = async (req: Request, res: Response) => {
    // No es necesario verificar si existen los datos
    // Dado que en los middlewares de la petición se realiza dicha validación
    try {
        const { email, password }: UsuarioI = req.body;
        const usuario = await Usuario.findOne({
            where: {
                email
            }
        });
        if (usuario) {
            return res.status(400).json({
                ok: false,
                msg: mensajesRespuesta.emailRegistered
            });
        }
        // Encriptar la contraseña
        const parsedPassword = bcrypt.hashSync(password, bcrypt.genSaltSync());
        // Guardar Usuario en BD
        const usuarioDB = await Usuario.create({ email: email.toLowerCase(), password: parsedPassword });
        return res.status(200).json({
            ok: true,
            usuario: usuarioDB.mapped()
        });
    } catch (error) {
        console.log(`${error}`.red);
        return res.status(500).json({
            ok: false,
            msg: mensajesRespuesta.status500
        })
    }
};

// Esto es un agregado para darle una mayor funcionalidad al proyecto
export const renew = async (req: Request, res: Response) => {
    try {
        const id = obtenerIdRequest(req);
        // Obtener el usuario por ID
        const resp = await Usuario.findByPk(id);
        if (!resp) {
            return res.json({
                ok: false,
                msg: 'No se pudo encontrar el usuario'
            });
        }
        const usuario = resp.mapped();
        // Generar un nuevo JWT
        const token = await generarJWT(id.toString());
        return res.json({
            ok: true,
            msg: 'Renew Correcto',
            usuario,
            token
        });
    } catch (error) {
        console.log(error);
        return res.status(500).json({
            ok: false,
            msg: 'Ha ocurrido un error interno'
        });
    }
};