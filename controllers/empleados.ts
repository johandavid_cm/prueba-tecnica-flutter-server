import { Request, Response } from "express";
import * as Sequelize from "sequelize";
import db from "../database/config";
import Area from "../models/db/area";
import Empleado, { EmpleadoI } from "../models/db/empleado";
import Pais from "../models/db/pais";
import TipoIdentificacion from "../models/db/tipoidentificacion";
import { generarEmail } from "../utils/generaremail";
import { mensajesRespuesta } from "../utils/mensajes";

export const crearEmpleado = async(req: Request, res: Response) => {
    const tempEmpleado: EmpleadoI = req.body;
    const transaction = await db.transaction();
    try {
        const empleado = Empleado.build(tempEmpleado);
        const pais = await Pais.findByPk(empleado.paisId, { transaction });
        if(!pais){
            return res.status(404).json({
                ok: false,
                msg: 'No se ha encontrado el pais'
            });
        }
        empleado.email = generarEmail(tempEmpleado, pais);

        let existe = true;
        let i = 1;
        while (existe) {
            const emp = await Empleado.findOne({ where: { email: empleado.email }, transaction });
            if(emp){
                existe = true;
                // Separo el email del empleado por el @ para verficar si se ha creado con algún número previamente
                const splited = empleado.email.split('@');
                const splitedId = splited[0].split(empleado.numeroIdentificacion);
                // Si spliteo el array por el numero de identificacion y el lenght es mayor a uno
                // Quiere decir que se ha asignado un número previamente
                // Ese numero lo extraigo y le sumo uno y procede a realizar la verificación nuevamente
                if(splitedId.length > 1){
                    const num: number = (+splitedId[1])+1;
                    empleado.email = `${splitedId[0]}${empleado.numeroIdentificacion}${num}@${splited[1]}`;
                } else {
                    empleado.email = `${splited[0]}${i}@${splited[1]}`;
                }
                i++;
            } else {
                existe = false;
            }
        }
        empleado.primerApellido = empleado.primerApellido.toUpperCase();
        empleado.primerNombre = empleado.primerNombre.toUpperCase();
        empleado.segundoApellido = empleado.segundoApellido.toUpperCase();
        empleado.otrosNombres = empleado.otrosNombres?.toUpperCase();
        await empleado.save({ transaction });
        await transaction.commit();
        return res.status(200).json({
            ok: true,
            empleado,
        });
    } catch (error) {
        await transaction.rollback();
        if(error instanceof Sequelize.ForeignKeyConstraintError){
            return res.status(400).json({
                ok: false,
                msg: 'Claves mal formadas, por favor verifique los datos, si el problema persiste, comuniquese con el administrador',
                error: error.message,
            });
        }
        if(error instanceof Sequelize.ValidationError){
            return res.status(400).json({
                ok: false,
                msg: 'Ha ocurrido un error con la validación de datos.',
                errors: error.errors[0].message,
            });
        }
        console.log(error);
        
        //ForeignKeyConstraintError
        //SequelizeForeignKeyConstraintError
        return res.status(500).json({
            ok: false,
            msg: 'Ha ocurrido un error inesperado'
        })
    }
};

export const getEmpleados = async(req: Request, res: Response) => {
    try {
        // Defino los items por página que quiero mostrar
        const itemsxPage = 10;
        const page = req.query.page;
        // Válido que el dato que obtenga sea de tipo string
        if(!page || Array.isArray(page) || typeof page !== 'string'){
            return res.status(400).json({
                ok: false,
                msg: 'Debe de definir la página'
            });
        }
        // Verífico el offset de donde comenzarán a cargar los datos
        const offset: number = +page * itemsxPage - itemsxPage;
        const empleados = await Empleado.findAll({ include: [TipoIdentificacion, Pais, Area], limit: itemsxPage, offset, });
        return res.status(200).json({
            ok: true,
            msg: 'Empleados obtenidos correctamente',
            empleados
        })
    } catch (error) {
        console.log(error);
        return res.status(500).json({
            ok: false,
            msg: mensajesRespuesta.status500,
        });
    }
};

export const eliminarEmpleado = async(req: Request, res: Response) => {
    try {
        const idEmpleado = req.params.idEmpleado;
        if(!idEmpleado){
            return res.status(400).json({
                ok: false,
                msg: 'No se ha enviado el id del empleado'
            });
        }
        const empleado = await Empleado.findByPk(idEmpleado);
        if(!empleado){
            return res.status(404).json({
                ok: false,
                msg: 'No se ha encontrado el empleado a eliminar'
            });
        }
        await empleado.destroy();
        return res.status(200).json({
            ok: true,
            msg: 'Se ha eliminado el usuario correctamente'
        })
    } catch (error) {
        return res.status(500).json({
            ok: false,
            msg: mensajesRespuesta.status500
        });
    }
};