import { Request, Response, NextFunction } from 'express';
import jwt from 'jsonwebtoken';

export const validarJWT = ( req: Request, res: Response, next: NextFunction ) => {
    try {
        const token = req.header('x-token');
        if(!token){
            return res.status(401).json({
                ok: false,
                msg: 'No hay token en la petición'
            });
        }
        if(process.env.JWT_KEY){
            const resp = jwt.verify(token, process.env.JWT_KEY);
            for (const [clave, valor] of Object.entries(resp)) {
                if(clave === 'id'){
                    Object.assign(req, { id: valor });
                }
            }
            next();
            return;
        }
        return res.status(500).json({
            ok: false,
            msg: 'Problema consulte con el admin'
        });
    } catch (error) {
        return res.status(400).json({
            ok: false,
            msg: 'Token no es válido'
        });
    }
};