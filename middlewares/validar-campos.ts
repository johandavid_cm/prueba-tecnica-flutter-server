import { Request, Response, NextFunction } from "express";
import { validationResult } from "express-validator";

export const validarCampos = (req: Request, res: Response, next: NextFunction) => {
    const errores = validationResult( req );
    if(!errores.isEmpty()){
        return res.status(400).json({
            ok: false,
            msg: 'Han ocurrido 1 o más errores durante la validación de datos, por favor verifique',
            errors: errores.mapped()
        });
    }
    next();
    return;
};