INSERT INTO `area` (`id`, `nombre`) VALUES (NULL, 'Financiera');
INSERT INTO `area` (`id`, `nombre`) VALUES (NULL, 'Administración');
INSERT INTO `area` (`id`, `nombre`) VALUES (NULL, 'Infraestructura');
INSERT INTO `area` (`id`, `nombre`) VALUES (NULL, 'Operación');
INSERT INTO `area` (`id`, `nombre`) VALUES (NULL, 'Talento Humano');
INSERT INTO `area` (`id`, `nombre`) VALUES (NULL, 'Servicios Varios');

INSERT INTO `tipoidentificacion` (`id`, `codigo`, `nombre`) VALUES (NULL, 'CC', 'Cédula de ciudadanía');
INSERT INTO `tipoidentificacion` (`id`, `codigo`, `nombre`) VALUES (NULL, 'CE', 'Cédula de extranjería');
INSERT INTO `tipoidentificacion` (`id`, `codigo`, `nombre`) VALUES (NULL, 'NIT', 'Número de Identificación Tributaria');

INSERT INTO `pais` (`id`, `nombre`, `codigo`) VALUES (NULL, 'Colombia', 'co');
INSERT INTO `pais` (`id`, `nombre`, `codigo`) VALUES (NULL, 'Estados Unidos', 'us');
