import path from "path";
import { SequelizeOptions } from "sequelize-typescript";
import dotenv from 'dotenv';
import { Sequelize } from 'sequelize-typescript'
import 'colors';
dotenv.config();

// Localizacion en donde se almacenan los modelos de la BD
const pathModels = path.join(__dirname, '../models/db');
// Si se corre sin crear la BD generara un error
// No es necesario crear los campos, solo la BD, Sequelize se encarga del resto

// Configurar el servidor en donde estará la BD
const config: SequelizeOptions = {
    // Sino esta en el entorono de desarrollo conectar a la BD de producción
    database: process.env.NODE_ENV === 'development' ? 'flutter_prueba_tec' : 'flutter_prueba_tec',
    models: [pathModels],
    logging: false,
    host: 'localhost',
    username: 'admin',
    password: '=4LXjZ0TL^Y$CMSm',
    dialect: 'mariadb',
    port: 3306,
};

const db = new Sequelize(config);
console.log(`Conectando a ${ config.database }...`.blue);

export default db;